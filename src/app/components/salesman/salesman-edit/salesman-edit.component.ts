import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';
import { ICrudResponse } from '../../../interfaces/crud-response.interface';
import { SalesmanService } from '../../../services/salesman.service';
import { MessageService } from '../../../services/message.service';

@Component({
  selector: 'app-salesman-edit',
  templateUrl: './salesman-edit.component.html',
  styleUrls: ['./salesman-edit.component.scss']
})
export class SalesmanEditComponent extends SelfUnsubscribe implements OnInit, OnDestroy {

  salesmanName: string;
  salesmanEntity: User;
  editStatus: number;
  status = {
    'ownContent': 1,
    'notOwnContent': 2,
    'newContent': 3
  };
  changePasswordState = false;
  password = {
    currentPassword: '',
    newPassword: '',
    confirmPassword: ''
  };

  constructor(
    private userService: UserService,
    private salesmanService: SalesmanService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    const data = this.route.snapshot.data;

    if (data.ownContent) {
      this.editStatus = this.status.ownContent;
      const userSubscr = this.userService.getUser().subscribe((user: User) => {
        this.salesmanEntity = new User({ ... user });
      });
      this.addSubscription(userSubscr);
    }

    if (!data.ownContent && !data.newContent && 'id' in this.route.snapshot.params) {
      this.editStatus = this.status.notOwnContent;
      const userID = +this.route.snapshot.params.id;
      const userSubscr = this.userService.loadUser(userID).subscribe((user: User) => {
        this.salesmanEntity = user;
        this.salesmanName = user.firstName + ' ' + user.lastName;
      });
      this.addSubscription(userSubscr);
    }

    if (data.newContent) {
      this.editStatus = this.status.newContent;
      this.salesmanEntity = new User({role: this.userService.getRole('SALES')});
    }
  }

  onSubmit() {
    let passToSend = null;

    if ((this.changePasswordState && this.password.newPassword) || this.editStatus === this.status.newContent) {
      passToSend = this.password;
    }

    if (this.editStatus === this.status.newContent) {
      // New user
      const subscr = this.salesmanService.createSalesman(this.salesmanEntity, passToSend).subscribe(
        (response: ICrudResponse) => {
          if (response.status === 1) {
            this.router.navigate(['/salesmans']);
          } else {
            this.messageService.showMessage(response.message, 'danger');
          }
        }
      );
    } else {
      // Update user
      const subscr = this.salesmanService.updateSalesman(this.salesmanEntity, passToSend).subscribe(
        (response: ICrudResponse) => {
          if (response.status === 1) {
            if (this.editStatus === this.status.ownContent) {
              this.userService.setUser(this.salesmanEntity);
              this.router.navigate(['/dashboard']);
            } else {
              this.router.navigate(['/salesmans']);
            }
          } else {
            this.messageService.showMessage(response.message, 'danger');
          }
        }
      );

      this.addSubscription(subscr);
    }
  }

  onChangePassword() {
    this.changePasswordState = !this.changePasswordState;
    if (this.editStatus === this.status.notOwnContent) {
      delete this.password.currentPassword;
    }
  }

  ngOnDestroy() {
    this.dispose();
  }
}
