import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { LicenseType } from '../../../models/license-type.model';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { LicenseTypesService } from '../../../services/license-types.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';

@Component({
  selector: 'app-license-types',
  templateUrl: './license-types.component.html',
  styleUrls: ['./license-types.component.scss']
})
export class LicenseTypesComponent extends SelfUnsubscribe implements OnInit, OnDestroy {
  displayedColumns = ['position', 'periodDays', 'description', 'actions'];
  dataSource: MatTableDataSource<LicenseType>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private licenseTypesService: LicenseTypesService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    super();
  }

  ngOnInit() {
    this.getLicenseTypes();
  }

  getLicenseTypes(): void {
    const slsubscr = this.licenseTypesService.getLicenseTypes()
      .subscribe((licenseTypesList: LicenseType[]) => {
        licenseTypesList.map((item, index) => {
          item.position = ++index;
        });

        this.dataSource = new MatTableDataSource(licenseTypesList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });

    this.addSubscription(slsubscr);
  }

  deleteLicenseType(id: number) {
    const deleteSubscr = this.licenseTypesService.deleteLicenseType(id).subscribe((response: boolean) => {
      if (response) {
        this.getLicenseTypes();
      }
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();

    this.dataSource.filter = filterValue;
  }

  ngOnDestroy() {
    this.licenseTypesService.dispose();
    this.dispose();
  }
}
