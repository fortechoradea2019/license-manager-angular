import { Component, OnInit, OnDestroy } from '@angular/core';
import { LicenseType } from '../../../models/license-type.model';
import { LicenseTypesService } from '../../../services/license-types.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';
import { NgForm } from '@angular/forms';
import { MessageService } from '../../../services/message.service';

@Component({
  selector: 'app-license-types-edit',
  templateUrl: './license-types-edit.component.html',
  styleUrls: ['./license-types-edit.component.scss']
})
export class LicenseTypesEditComponent extends SelfUnsubscribe implements OnInit, OnDestroy {
  licenseTypeEntity: LicenseType;

  constructor(
    private licenseTypesService: LicenseTypesService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService
  ) {
    super();
  }

  ngOnInit() {
    const params = this.route.snapshot.params;

    if ('id' in params) {
      this.getLicenseType(+params.id);
    }
  }

  getLicenseType(id: number): void {
    const ltSubscr = this.licenseTypesService.getLicenseType(id)
      .subscribe((licenseType: LicenseType) => {
        this.licenseTypeEntity = licenseType;
      });

    this.addSubscription(ltSubscr);
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      form.value.id = this.licenseTypeEntity.id;
      const licenseTypeSubscr = this.licenseTypesService.updateLicenseType(form.value, this.licenseTypeEntity.id)
        .subscribe((response) => {
          licenseTypeSubscr.unsubscribe();
          if (response) {
            this.messageService.showMessage('License type successfully updated!', 'success');
            this.router.navigate(['/license-types'], {relativeTo: this.route});
          }
        });
      this.addSubscription(licenseTypeSubscr);
    }
  }

  ngOnDestroy() {
    this.licenseTypesService.dispose();
    this.dispose();
  }
}
