import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LicenseTypesComponent } from './license-types/license-types.component';
import { LicenseTypesCreateComponent } from './license-types-create/license-types-create.component';
import { LicenseTypesEditComponent } from './license-types-edit/license-types-edit.component';
import { Routes, RouterModule } from '@angular/router';
import { AppLayoutComponent } from '../../shared/layout/app/app.component';
import { LoggedInGuard } from '../../services/guards/logged-in.service';
import { IsAdminGuard } from '../../services/guards/is-admin.service';
import {
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatTableModule,
  MatPaginatorModule
} from '@angular/material';
import { ModalConfirmationModule } from '../../shared/modal/confirmation/confirmation.module';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      { path: 'license-types', component: LicenseTypesComponent, canActivate: [LoggedInGuard, IsAdminGuard] },
      { path: 'license-types/create', component: LicenseTypesCreateComponent, canActivate: [LoggedInGuard, IsAdminGuard] },
      { path: 'license-types/:id/edit', component: LicenseTypesEditComponent, canActivate: [LoggedInGuard] }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    ModalConfirmationModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LicenseTypesComponent,
    LicenseTypesCreateComponent,
    LicenseTypesEditComponent
  ]
})
export class LicenseTypesModule { }
