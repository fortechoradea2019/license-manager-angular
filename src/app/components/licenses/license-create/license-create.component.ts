import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SelfUnsubscribe } from '../../../shared/self-unsubscribe';
import { LicenseService } from '../../../services/license.service';
import { ClientService } from '../../../services/client.service';
import { Client } from '../../../models/client.model';
import { LicenseType } from '../../../models/license-type.model';
import { License } from '../../../models/license.model';
import * as moment from 'moment';
import { MessageService } from '../../../services/message.service';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { MatDatepickerInputEvent } from '@angular/material';
import { LicenseTypesService } from '../../../services/license-types.service';

@Component({
  selector: 'app-license-create',
  templateUrl: './license-create.component.html',
  styleUrls: ['./license-create.component.scss']
})
export class LicenseCreateComponent extends SelfUnsubscribe implements OnInit, OnDestroy {

  clientID: number;
  minDate: Date;
  licenseTypes: LicenseType[] = [];
  date = {
    begin: new Date,
    end: new Date
  };
  license = new License({
    clientId: 0,
    type: {},
    k1: '',
    k2: '',
    start: '',
    end: '',
    period: 0
  });

  constructor(
    private clientService: ClientService,
    private licenseService: LicenseService,
    private licenseTypesService: LicenseTypesService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    calendar: NgbCalendar
  ) {
    super();
    this.license.start = new Date();
    this.minDate = new Date();

    if (this.license.period !== 0) {
      this.license.end = new Date(this.license.start);
      this.license.end.setDate(this.license.end.getDate() + this.license.period);
    }
  }

  ngOnInit() {
    const subscr = this.route.params.subscribe((params: Params) => {
      this.clientID = +params.id;
    });
    this.addSubscription(subscr);

    // Get license types
    this.getLicenseTypes();

    // Prepare license
    this.license.clientId = this.clientID;
  }

  onLicenseTypeChange(id) {
    const slsubscr = this.licenseTypesService.getLicenseType(id)
      .subscribe((licenseType: LicenseType) => {

        this.license.period = licenseType.periodDays;
        this.license.start = new Date();
        this.license.type.id = licenseType.id;

        this.updateLicenseExpiration();
      });

    this.addSubscription(slsubscr);
  }

  onDateChange(event: MatDatepickerInputEvent<Date>) {
    this.license.start = event.value['begin'];
    this.license.end = event.value['end'];

    this.getLicensePeriod();
  }

  onPeriodChange(value) {
    this.license.period = +value;

    this.updateLicenseExpiration();
  }

  getLicensePeriod() {
    const startDate = this.license.start;
    const endDate = this.license.end;
    const timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
    const nrOfDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    this.license.period = nrOfDays;
  }

  updateLicenseExpiration() {
    this.license.end = new Date(this.license.start);
    this.license.end.setDate(this.license.end.getDate() + this.license.period);

    this.updateDate();
  }

  updateDate() {
    this.date = {
      begin: this.license.start,
      end: this.license.end
    };
  }

  getLicenseTypes(): void {
    const slsubscr = this.licenseTypesService.getLicenseTypes()
      .subscribe((licenseTypesList: LicenseType[]) => {
        this.licenseTypes = licenseTypesList;
      });

    this.addSubscription(slsubscr);
  }

  onSubmit(form: NgForm) {
    if (form.valid) {
      const secretKey = this.licenseService
        .generateSecretKey(
          this.license.k1,
          this.license.start,
          this.license.end
        );

      if (!secretKey) {
        this.messageService.showMessage('Problem with K1', 'danger');
        return false;
      }

      this.license.k2 = secretKey;

      const licenseSubscr = this.licenseService.createLicense(this.license).subscribe((response) => {
        licenseSubscr.unsubscribe();
        if (response) {
          this.router.navigate(['..'], {relativeTo: this.route});
        }
      });
      this.addSubscription(licenseSubscr);
    }
  }

  ngOnDestroy() {
    this.dispose();
  }
}
