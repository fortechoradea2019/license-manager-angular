import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LicenseComponent } from './license/license.component';
import { LicenseCreateComponent } from './license-create/license-create.component';
import { LoggedInGuard } from '../../services/guards/logged-in.service';
import { FormsModule } from '@angular/forms';
import { ModalConfirmationModule } from '../../shared/modal/confirmation/confirmation.module';
import { LimitToModule } from '../../pipes/limit-to/limit-to.module';
import { UntilNowModule } from '../../pipes/until-now/until-now.module';
import { LicenseTypeModule } from '../../pipes/license-type/license-type.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  MatButtonModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatListModule,
  MatIconModule,
  MatSelectModule,
  MatTooltipModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatBadgeModule
} from '@angular/material';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LicenseEditComponent } from './license-edit/license-edit.component';
import { AppLayoutComponent } from '../../shared/layout/app/app.component';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      { path: 'clients/:id/licenses', component: LicenseComponent, canActivate: [LoggedInGuard] },
      { path: 'clients/:id/licenses/create', component: LicenseCreateComponent, canActivate: [LoggedInGuard] },
      { path: 'licenses/:id/edit', component: LicenseEditComponent, canActivate: [LoggedInGuard] }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    ModalConfirmationModule,
    LimitToModule,
    UntilNowModule,
    LicenseTypeModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatTooltipModule,
    MatBadgeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SatDatepickerModule,
    SatNativeDateModule,
    RouterModule.forChild(routes),
    NgbModule.forRoot()
  ],
  declarations: [
    LicenseComponent,
    LicenseCreateComponent,
    LicenseEditComponent
  ],
  providers: [
    LoggedInGuard
  ]
})
export class LicensesModule { }
