import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoggedInGuard } from '../../services/guards/logged-in.service';
import { IsAnonymousGuard } from '../../services/guards/is-anonymous.service';
import { IsAdminGuard } from '../../services/guards/is-admin.service';
import { IsSalesmanGuard } from '../../services/guards/is-salesman.service';
import { DashboardComponent } from './dashboard.component';
import { AppLayoutComponent } from '../../shared/layout/app/app.component';
import { LicensesTableComponent } from './components/licenses-table/licenses-table.component';
import { ModalConfirmationModule } from 'src/app/shared/modal/confirmation/confirmation.module';
import { MatTableModule, MatButtonModule, MatInputModule, MatSortModule, MatPaginatorModule, MatTooltipModule } from '@angular/material';
import { UntilNowModule } from 'src/app/pipes/until-now/until-now.module';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent, canActivate: [LoggedInGuard] },
      { path: 'license-table', component: LicensesTableComponent, canActivate: [LoggedInGuard, IsSalesmanGuard] },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    ModalConfirmationModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatTooltipModule,
    UntilNowModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    DashboardComponent,
    LicensesTableComponent
  ],
  providers: [
    LoggedInGuard,
    IsAnonymousGuard,
    IsAdminGuard,
    IsSalesmanGuard
  ]
})
export class DashboardModule { }
