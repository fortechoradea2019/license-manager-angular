import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { License } from 'src/app/models/license.model';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-licenses-table',
  templateUrl: './licenses-table.component.html',
  styleUrls: ['./licenses-table.component.scss']
})
export class LicensesTableComponent implements OnInit {

  @Input() licenses: License[];

  dataSource: MatTableDataSource<License>;
  displayedColumns = ['position', 'client', 'start', 'end'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
  ) {  }

  ngOnInit() {
      this.licenses.map((item, index) => {
        item.position = index + 1;
      });

      this.dataSource = new MatTableDataSource(this.licenses);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  }
}
