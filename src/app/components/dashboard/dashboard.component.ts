import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';
import {Subscription} from 'rxjs';
import {SelfUnsubscribe} from '../../shared/self-unsubscribe';
import { License } from 'src/app/models/license.model';
import { LicenseService } from 'src/app/services/license.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends SelfUnsubscribe implements OnInit, OnDestroy {

  todayLicenses: License[] = [];
  weekLicenses: License[] = [];
  monthLicenses: License[] = [];
  user: User;

  constructor(
    private userService: UserService,
    private licenseService: LicenseService,
    ) {
    super();
  }

  ngOnInit() {
    const userScrb = this.userService.getUser()
      .subscribe(
        (user: User) => {
          this.user = user;
        }
      );

    this.addSubscription(userScrb);

    const licenseSubscrToday = this.licenseService.getLicensesByInterval(0, 1)
    .subscribe(
      (licenses: License[]) => {
        this.todayLicenses = licenses;
      }
    );

  this.addSubscription(licenseSubscrToday);

  const licenseSubscrWeek = this.licenseService.getLicensesByInterval(0, 7)
  .subscribe(
    (licenses: License[]) => {
      this.weekLicenses = licenses;
    }
  );
  this.addSubscription(licenseSubscrWeek);

  const licenseSubscrMonth = this.licenseService.getLicensesByInterval(0, 30)
  .subscribe(
    (licenses: License[]) => {
      this.monthLicenses = licenses;
    }
  );
  this.addSubscription(licenseSubscrMonth);
}

  ngOnDestroy() {
    this.dispose();
  }
}
