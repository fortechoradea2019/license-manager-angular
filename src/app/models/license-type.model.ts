export class LicenseType {
  id: number;
  description: string;
  periodDays: number;
  position: number;

  constructor(data: any) {
    for (const prop in data) {
      if (data.hasOwnProperty(prop)) {
        this[prop] = data[prop];
      }
    }
  }
}
