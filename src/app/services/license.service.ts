import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { MessageService } from './message.service';
import { Injectable } from '@angular/core';
import { SelfUnsubscribe } from '../shared/self-unsubscribe';
import { RequestManager } from './request-manager.service';
import { License } from '../models/license.model';
import { LicenseType } from '../models/license-type.model';

@Injectable({
  providedIn: 'root'
})
export class LicenseService extends SelfUnsubscribe {

  licenseTypes: LicenseType[] = [];

  constructor(
    private messageService: MessageService,
    private requestManager: RequestManager
  ) {
    super();

    const subscr = this.requestManager.getLicenseTypes().subscribe((response) => {
      for (const data of response) {
        this.licenseTypes.push(new LicenseType(data));
      }
    });
    this.addSubscription(subscr);
  }

  getLicenseType(licenseName: string): LicenseType | boolean {
    const type = this.licenseTypes.filter((lt) => lt.description === licenseName).pop();
    return type || false;
  }

  getLicenses(clientID: number): Observable<License[]> {
    return new Observable<License[]>((observer: Observer<License[]>) => {
      const subscr = this.requestManager.getLicenses(clientID)
        .subscribe(
          (response) => {
            const licenses: License[] = [];
            for (const licenseData of response) {
              licenseData.start = new Date(licenseData.start).getTime();
              licenseData.end = new Date(licenseData.end).getTime();
              licenses.push(new License(licenseData));
            }
            observer.next(licenses);
          },
          (err) => {
            observer.next([]);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }

  getAllLicensesBySalesman(id: number): Observable<any> {
    return new Observable<any>((observer: Observer<any>) => {
      const subscr = this.requestManager.getAllLicensesBySalesman(id)
        .subscribe(
          (response: any) => {

            observer.next(response);
          },
          (err) => {
            observer.next([]);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );
      this.addSubscription(subscr);
    });
  }

  createLicense(license: License): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      const subscr = this.requestManager.createLicense(license)
        .subscribe(
          (response) => {
            observer.next(true);
          },
          (err) => {
            observer.next(false);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );
      this.addSubscription(subscr);
    });
  }

  updateLicense(license: License, id: number): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      const subscr = this.requestManager.updateLicense(license, id)
        .subscribe(
          (response) => {
            observer.next(true);
          },
          (err) => {
            observer.next(false);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );
      this.addSubscription(subscr);
    });
  }

  getLicense(licenseID: number): Observable<License> {
    return new Observable<License>((observer: Observer<License>) => {
      const subscr = this.requestManager.getLicense(licenseID)
        .subscribe(
          (response) => {
            const license = new License(response);
            observer.next(license);
          },
          (err) => {
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }

  generateSecretKey(key: string, start: any, end: any): string {
    try {
      const data = {
        key: key,
        start: Math.round(+start),
        end: Math.round(+end)
      };

      return this.encriptKey(data);
    } catch (err) {
      this.messageService.showMessage(err.message, 'danger');
    }
  }

  decriptKey(key: string) {
    return JSON.parse(key);
  }

  encriptKey(data: any): string {
    return JSON.stringify(data);
  }

  deleteLicense(id: number): Observable<boolean> {
    return new Observable<boolean>((observer: Observer<boolean>) => {
      const subscr = this.requestManager.deleteLicense(id)
        .subscribe(
          (response) => {
            observer.next(true);
          },
          (err) => {
            observer.next(false);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );

      this.addSubscription(subscr);
    });
  }

  getLicensesByInterval(start: number, end: number): Observable<License[]> {
    return new Observable<License[]>((observer: Observer<License[]>) => {
      const subscr = this.requestManager.getAllLicensesByInterval(start, end)
        .subscribe(
          (response) => {
            const licenses: License[] = [];
            for (const licenseData of response) {
              licenseData.start = new Date(licenseData.start).getTime();
              licenseData.end = new Date(licenseData.end).getTime();
              licenses.push(new License(licenseData));
            }
            observer.next(licenses);
          },
          (err) => {
            observer.next([]);
            this.messageService.showMessage(err.error.message, 'danger');
          }
        );
        this.addSubscription(subscr);
      });
    }
}
