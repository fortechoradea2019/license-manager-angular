import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

import { DummyClients } from '../dummy/clients';
import { DummyLicenses } from '../dummy/licenses';
import { DummyLicensesTypes } from '../dummy/licenses-types';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestManager {

  dummyClients = new DummyClients();
  dummyLicenses = new DummyLicenses();
  dummyLicenseTypes = new DummyLicensesTypes();

  constructor(
    private http: HttpClient
  ) { }

  // AUTH REQUESTS

  loginUser(email: string, password: string): Observable<any> {
    const data = {
      'username': email,
      'password': password
    };
    return this.http.post(`${environment.API_URL}/sign-in`, data);
  }


  logoutUser(token: string): Observable<any> {
    // TODO: Call backend to delete hash
    return new Observable<any>((observer: Observer<any>) => {
      observer.next({status: 1});
      observer.complete();
    });
  }

  loadUser(id: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/user/${id}`);
  }

  updateUser(userEntity: any, password?: any): Observable<any> {
    const data = userEntity;
    data.changePassword = {};

    if (password) {
      data.changePassword = password;
    }
    delete data.password;
    return this.http.patch(`${environment.API_URL}/user/${userEntity.id}`, data);
  }

  createUser(userEntity: any, password: any): Observable<any> {
    const data = userEntity;
    data.changePassword = password;

    return this.http.post(`${environment.API_URL}/user`, data);
  }

  // ==========================================================================

  // SALESMAN

  getSalesmanList(): Observable<any> {
    return this.http.post(`${environment.API_URL}/user/listSalesManByTerm`, {term: ''});
  }

  getSalemsmanFiltered(term: string): Observable<any> {
    return this.http.post(`${environment.API_URL}/user/listSalesManByTerm`, {term: term});
  }

  deleteSalesman(id: number): Observable<any> {
    return this.http.delete(`${environment.API_URL}/user/${id}`);
  }
  // ==========================================================================

  // CLIENT

  getClientsList(salesmanID: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/client/listByUserId/${salesmanID}`).map((data: any[]) => {
      for (const d of data) {
        if (typeof d.userId === 'object') {
          d.userId = d.userId.id;
        }
      }
      return data;
    });
  }

  getClient(clientID: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/client/${clientID}`).map((data: any) => {

      if (typeof data.userId === 'object') {
        data.userId = data.userId.id;
      }

      return data;
    });
  }

  createClient(client: any): Observable<any> {
    const data = {... client};

    if (typeof data.userId !== 'object') {
      data.userId = { id: data.userId };
    }

    return this.http.post(`${environment.API_URL}/client`, data);
  }

  updateClient(client: any, id: number): Observable<any> {
    const data = {... client};

    if (typeof data.userId !== 'object') {
      data.userId = { id: data.userId };
    }

    return this.http.patch(`${environment.API_URL}/client/${id}`, data);
  }

  deleteClient(id: number): Observable<any> {
    return this.http.delete(`${environment.API_URL}/client/${id}`);
  }
  // ==========================================================================

  // ROLES

  getRoles() {
    return new Observable<any>((observer: Observer<any>) => {
      const dummyRoles = {
        'ADMIN': 1,
        'SALES': 2
      };
      observer.next(dummyRoles);
      observer.complete();
    });
  }
  // ==========================================================================

  // LICENSE

  getLicenses(clientID: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/license/listByClientId/${clientID}`);
  }

  getLicense(licenseID: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/license/${licenseID}`);
  }

  getAllLicensesBySalesman(salesmanID: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/license/listBySalesManId/${salesmanID}`);
  }

  createLicense(license: any): Observable<any> {
    const data = {... license};

    if (typeof data.clientId !== 'object') {
      data.clientId = { id: data.clientId };
    }

    return this.http.post(`${environment.API_URL}/license`, data);
  }

  deleteLicense(id: number): Observable<any> {
    return this.http.delete(`${environment.API_URL}/license/${id}`);
  }

  updateLicense(license: any, id: number): Observable<any> {
    const data = {... license};

    if (typeof data.clientId !== 'object') {
      data.clientId = { id: data.clientId };
    }

    return this.http.patch(`${environment.API_URL}/license/${id}`, data);
  }

  getAllLicensesByInterval(start: number, end: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/license/listByInterval?start=${start}&end=${end}`);
  }
  // ==========================================================================

  createLicenseType(licenseType: any): Observable<any> {
    const data = {... licenseType};

    return this.http.post(`${environment.API_URL}/license-type`, data);
  }

  getLicenseTypes(): Observable<any> {
    return this.http.get(`${environment.API_URL}/license-type`);
  }

  deleteLicenseType(id: number): Observable<any> {
    return this.http.delete(`${environment.API_URL}/license-type/${id}`);
  }

  getLicenseType(licenseTypeID: number): Observable<any> {
    return this.http.get(`${environment.API_URL}/license-type/${licenseTypeID}`);
  }

  updateLicenseType(licenseType: any, id: number): Observable<any> {
    const data = {... licenseType};

    return this.http.patch(`${environment.API_URL}/license-type/${id}`, data);
  }
}
