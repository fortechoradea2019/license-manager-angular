import { TestBed, inject } from '@angular/core/testing';

import { LicenseService } from './license.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LicenseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [LicenseService]
    });
  });

  it('should be created', inject([LicenseService], (service: LicenseService) => {
    expect(service).toBeTruthy();
  }));
});
