import { Pipe, PipeTransform } from '@angular/core';
import {LicenseService} from '../../services/license.service';
import {LicenseType} from '../../models/license-type.model';

@Pipe({
  name: 'licenseType'
})
export class LicenseTypePipe implements PipeTransform {

  constructor(
    private licenseService: LicenseService
  ) { }

  transform(id: string, args?: any): string {
    let lt = this.licenseService.getLicenseType(id);
    if (lt) {
      lt = <LicenseType>lt;
      return lt.description;
    } else {
      return '';
    }
  }

}
