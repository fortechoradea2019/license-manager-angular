import { browser, by, element } from 'protractor';

export class LoginPage {
  navigateTo() {
    return browser.get('/');
  }

  setUsername() {
    return element(by.name('email')).sendKeys('admin@license-manager.com');
  }

  setPassword() {
    return element(by.name('password')).sendKeys('admin1234');
  }

  onLogin() {
    return element(by.buttonText('Log in')).click();
  }

  getAppLoginLayout() {
    return element(by.tagName('app-login-layout'));
  }

  getAppLayout() {
    return element(by.tagName('app-app'));
  }
}
