import { LoginPage } from './login.po';

describe('workspace-project Login', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
  });

  it('should login and check logged app layout', () => {
    page.navigateTo();
    expect(page.getAppLoginLayout().isPresent()).toBe(true);
    page.setUsername();
    page.setPassword();
    page.onLogin();
    expect(page.getAppLoginLayout().isPresent()).toBe(false);
    expect(page.getAppLayout().isPresent()).toBe(true);
  });
});